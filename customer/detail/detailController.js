function printItem(res) {
  var contentHTML = ` <div class="products__items-left">
<img src="${res.data.hinhAnh}" alt="" />
</div>
<div class="products__items-right">
<div class="products__items-right-name">
  <h3>${res.data.tenSP}</h3>
</div>
<div class="products__items-right-size">
  <h3 class="products__items-right-size-heading">Available Size</h3>
  <div class="products__items-right-size-content">
    <button class="products__items-right-size-content-btn">38</button>
    <button class="products__items-right-size-content-btn">39</button>
    <button class="products__items-right-size-content-btn">40</button>
    <button class="products__items-right-size-content-btn">41</button>
    <button class="products__items-right-size-content-btn">42</button>
    <button class="products__items-right-size-content-btn">43</button>
  </div>
</div>
<div class="products__items-right-price">
  <h3>đ ${(res.data.giaSP).toLocaleString()}</h3>
</div>
<div class="products__items-right-cart">
  <button type="submit" id="btnAddToCart" onclick="btnAddToCart(${res.data.id})">ADD TO CART</button>
</div>
</div> `;
  document.getElementById("productsItem").innerHTML = contentHTML;
}

function renderCart(arrCart) {
  let contentHTML = " ";
  for (let i = 0; i < arrCart.length; i++) {
    let cart = arrCart[i];
    let contentCart = `<tr
    class="items-center border-b-2 py-5 w-full flex justify-evenly"
  >
    <td>
      <img
        src="${cart.hinhAnh}"
        alt=""
        class="products-img w-56"
      />
    </td>
    <td class="w-48">
      <span class="products-name">${cart.tenSP}</span>
    </td>
    <td class="flex">
      <button onclick="btnMinus(${cart.id})">
      <i class="fa fa-minus-circle text-3xl"></i>
      </button>
      <input
        type="text"
        value="${cart.soLuong}"
        class="mx-5 text-center w-10 bg-transparent"
      />
      <button onclick="btnPlus(${cart.id})"><i class="fa fa-plus-circle text-3xl"></i></button>
    </td>
    <td><span class="products-price">${cart.giaSP.toLocaleString()} đ</span></td>
    <td><span class="price-total">${cart.tongTien().toLocaleString()} đ</span></td>
    <td><button class="cart__item-btnRemove" onclick="btnRemove(${cart.id})">
    <i class="fa fa-trash-alt text-3xl"></i>
  </button></td>
  </tr>
  `;
  contentHTML += contentCart;
  }
  document.querySelector("#tbodyCart").innerHTML = contentHTML;
}

function products(_tenSP, _giaSP,_hinhAnh, _soLuong,_id) {
  this.tenSP = _tenSP;
  this.giaSP = _giaSP;
  this.hinhAnh = _hinhAnh;
  this.soLuong = _soLuong;
  this.id = _id;
  this.tongTien = function () {
    return this.giaSP * this.soLuong;
  };

}


function batLoading(){
  document.getElementById("loading").style.display="flex";
}
function tatLoading(){
  document.getElementById("loading").style.display="none";
}